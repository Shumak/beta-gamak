import React, { useCallback, useState } from 'react';
import "./Identification.css";

export const Identification = (props) => {
    const [textIdentification, SetTextIdentification] = useState("Вход");
    const [navigation, Changenavigation] = useState("navigator");

    /*          Функция, открывающая меню путем изменения названия класса           */
    const showIdentification = useCallback(() => {
        switch (navigation) {
            case "navigator":
                SetTextIdentification("");
                Changenavigation("navigator open");
                break;
            case "navigator open":
                SetTextIdentification("Вход");
                Changenavigation("navigator");
                break;
            default:
                alert("Непредвиденная ошибка. Обновите страницу!");
                break;
        }
    }, [navigation])

    return (
        <nav className={navigation}>
            <strong id="input">{textIdentification}</strong>

            <ul className="menu_authentication">
                {props.items.map((item, index) =>
                    <li key={index}>
                        <button onClick={() => props.FormsNames(item)}>
                            {item}
                        </button>
                    </li>)}
            </ul>

            <button className="burger" onClick={() => showIdentification()}>
                <div className="line1"></div>
                <div className="line2"></div>
                <div className="line3"></div>
            </button>

        </nav>
    );
}