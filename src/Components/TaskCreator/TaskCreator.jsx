import React, { useCallback, useState, useEffect } from 'react';
import { postTaskAsync } from '../../Services/Backend/Task/postTaskAsync';
import './TaskCreator.css';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

export const TaskCreator = (props) => {

    useEffect(() => {
        const day = new Date();
        setToday(day.toISOString().substring(0, 10));
    }, [])

    const [deadline, setDeadline] = useState(null);
    const [taskTitle, setTaskTitle] = useState();
    const [visibleDatepick, setVisibleDatepick] = useState(false);
    const [today, setToday] = useState();

    const { addNewTask, status } = props;

    const addnewtask = useCallback((event) => {
        if ((event.key === 'Enter' || event.key === undefined)
            && status === "noactive") {
            postTaskAsync(event.target.value, deadline)
                .then(json => addNewTask(json))
                .then(event.target.value = "");
        } else if (event.key === 'Enter' && status === "active") {
            setVisibleDatepick(true);
            setTaskTitle(event.target.value);
        }
    }, [addNewTask, deadline, status])

    return (
        <div className="taskCreator">
            {!visibleDatepick &&
                <input type="text"
                    onKeyPress={(event) => addnewtask(event)}
                    placeholder="Добавить задачу"
                    autoFocus
                />
            }
            {visibleDatepick &&
                <div className="activeTaskCreator">
                    < DatePicker
                        autoFocus
                        minDate={new Date()}
                        value={today}
                        onChange={(date) => {
                            setDeadline(date.getFullYear() + "-" + date.getMonth() + 1 + "-" + date.getDate());
                            postTaskAsync(taskTitle, date.getFullYear() + "-" + date.getMonth() + 1 + "-" + date.getDate())
                                .then(json => addNewTask(json));
                        }}
                    />
                </div>
            }
        </div>
    )
}