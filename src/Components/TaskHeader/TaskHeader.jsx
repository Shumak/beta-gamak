import React, { useCallback } from 'react';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

import { putTaskAsync } from '../../Services/Backend/Task/putTaskAsync';
import deleteImg from '../../Assets/Images/deleteimg.svg';
import tickImg from '../../Assets/Images/tick.svg';
import deteImg from '../../Assets/Images/date.svg';

import { deleteTaskAsync } from '../../Services/Backend/Task/deleteTaskAsync';


export const TaskHeader = (props) => {

    const ExampleCustomInput = ({ onClick }) => (
        <button className="changedatabutton">
            <img src={deteImg} alt="change" onClick={onClick} />
        </button>
    );

    const updateTaskTitle = useCallback((taskId, event) => {
        const taskTitle = event.target.value;

        if (event.key === 'Enter' || event.key === undefined) {
            putTaskAsync(taskId, taskTitle, props.task.deadline);
        }
    }, [props.task.deadline]);

    return (
        <div className="taskheader">
            <div className="dateandtitle">
                <div className={"blockstatus" + props.status}></div>
                {props.status !== "noactive" &&
                    <input
                        className="date"
                        type="text"
                        value={props.task.deadline}
                        readOnly />
                }
                <input
                    className="tasktitle"
                    type="text"
                    onKeyPress={(event) => updateTaskTitle(props.task.id, event)}
                    onBlur={(event) => updateTaskTitle(props.task.id, event)}
                    defaultValue={props.task.title}
                />
            </div>
            {props.mouseOverDiv &&
                <div className="buttons">
                    {props.status !== "done" &&
                        < DatePicker
                            withPortal
                            minDate={new Date()}
                            value={props.task.deadline}
                            onChange={date => {
                                putTaskAsync(props.task.id,
                                    props.task.title,
                                    (date.getFullYear() + "-" + date.getMonth() + 1 + "-" + date.getDate()))
                                    .then(json => props.updateTaskArray(props.task.id, json));
                            }}
                            customInput={<ExampleCustomInput />}
                        />
                    }

                    <button
                        className="changestatus"
                        onClick={() =>
                            deleteTaskAsync(props.task.id)
                                .then(props.deleteTaskArray(props.task.id))
                        }>
                        <img src={tickImg} alt="done" />
                    </button>

                    <button
                        className="delettaskebutton"
                        onClick={() =>
                            deleteTaskAsync(props.task.id)
                                .then(props.deleteTaskArray(props.task.id))
                        }>
                        <img src={deleteImg} alt="delete" />
                    </button>
                </div>
            }
        </div>
    )
}