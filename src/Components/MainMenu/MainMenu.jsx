import React, { useCallback, useMemo, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { CalendarRoute,  HomeRoute, TasksRoute } from '../../Routes';
import { logout } from '../../Services/Login/logoutAsync';
import './MainMenu.css';

export const MainMenu = (props) => {

    const [classNameMenu, setClassNameMenu] = useState("main_menu");
    const [burgerClassName, setBurgerClassName] = useState("burger");

    const { checkexit } = props;
    const history = useHistory();

    const openMenu = useCallback(() => {
        switch (classNameMenu) {
            case "main_menu":
                setClassNameMenu("main_menu open");
                setBurgerClassName("burger close");
                break;
            case "main_menu open":
                setClassNameMenu("main_menu");
                setBurgerClassName("burger");
                break;
            default:
                alert("Непредвиденная ошибка. Обновите страницу!");
                break;
        }

    }, [classNameMenu])

    const items = useMemo(() => {

        return ([{
            title: "Главная",
            url: HomeRoute,
        },
        {
            title: "Календарь",
            url: CalendarRoute,
        },
        {
            title: "Задачи",
            url: TasksRoute,
        }]);

    }, []);

    const onLogout = useCallback(() => {
        logout();
        history.replace(HomeRoute);
        checkexit(false);
    }, [history, checkexit]);

    return (
        <nav>
            <ul className={classNameMenu}>
                {items.map((item, index) =>
                    <li key={index}>
                        <a href={item.url}>{item.title}</a>
                    </li>)}
                <li><button id="exit" onClick={onLogout}>Выход</button></li>
            </ul>
            <button className={burgerClassName} onClick={() => openMenu()}>
                <div className="line1"></div>
                <div className="line2"></div>
                <div className="line3"></div>
            </button>
        </nav>
    );
}