import React, { useState } from 'react';
import { TaskHeader } from '../../TaskHeader/TaskHeader';
import { Subtasks } from './Subtasks/Subtasks';
import './TaskBlock.css';

export const TaskBlock = (props) => {

    const [mouseOverDiv, setMouseOverDiv] = useState(false);

    return (
        <div
            onMouseEnter={() => setMouseOverDiv(true)}
            onMouseLeave={() => setMouseOverDiv(false)}
            className={props.className}>
            <TaskHeader
                status={props.status}
                task={props.task}
                mouseOverDiv={mouseOverDiv}
                deleteTaskArray={taskId => props.deleteTaskArray(taskId)}
                updateTaskArray={(taskId, deadline) => props.updateTaskArray(taskId, deadline)}
            />
            <ul className="subtask_list">
                <Subtasks
                    taskId={props.task.id}
                    subtasks={props.task.subtasks}
                    deleteThisTask={taskId => props.deleteTaskArray(taskId)}
                />
            </ul>
        </div>
    )
}