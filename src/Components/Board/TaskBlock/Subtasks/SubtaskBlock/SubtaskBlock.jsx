import React, { useCallback, useEffect, useState } from 'react';
import { fetchSubtasksAsync } from '../../../../../Services/Backend/fetchSubtasksAsync';
import { deleteSubtaskAsync } from '../../../../../Services/Backend/Subtask/deleteSubtaskAsync';
import { putSubtaskAsync } from '../../../../../Services/Backend/Subtask/putSubtaskAsync';
import { putSubtaskStatusAsync } from '../../../../../Services/Backend/putSubtaskStatusAsync';
import deleteImg from '../../../../../Assets/Images/deleteimg.svg';
import tickImg from '../../../../../Assets/Images/tick.svg';
import './SubtaskBlock.css';
import { deleteTaskAsync } from '../../../../../Services/Backend/Task/deleteTaskAsync';

export const SubtaskBlock = (props) => {

    const [isLoading, setIsLoading] = useState(true);
    const [subtaskStatus, setSubtaskStatus] = useState();
    const [subtask, setSubtask] = useState();
    const [mouseOverDiv, setMouseOverDiv] = useState();

    const { deleteSubtaskArray, taskId, deleteThisTask } = props;

    useEffect(() => {
        setIsLoading(true);
        setSubtask(props.subtask);
        setSubtaskStatus(props.subtask.done);
        setIsLoading(false);
    }, [props.subtask, props.subtask.done])


    const checkAllSubtasks = useCallback(() => {
        fetchSubtasksAsync(taskId)
            .then(subtasks => {
                var isAnyOneActive = false;
                subtasks.map((subtask) => {
                    if (subtask.done === false) {
                        isAnyOneActive = true;
                        return;
                    }
                });
                if (isAnyOneActive === false) {
                    deleteTaskAsync(taskId)
                    .then(deleteThisTask(taskId));
                }
            });
    }, [taskId, deleteThisTask])

    const doneOrNote = useCallback(() => {
        if (subtaskStatus === true) {
            return "subtask done";
        } else {
            return "subtask";
        }
    }, [subtaskStatus]);

    const updateSubtaskTitle = useCallback((subtaskId, event) => {
        const subtaskTitle = event.target.value;

        if (event.key === 'Enter' || event.key === undefined) {
            putSubtaskAsync(subtaskId, subtaskTitle);
        }
    }, []);

    const changeSubtaskStatus = useCallback((subtaskId) => {
        putSubtaskStatusAsync(subtaskId, !subtaskStatus)
            .then(setSubtaskStatus(!subtaskStatus))
            .then(checkAllSubtasks());
    }, [subtaskStatus, checkAllSubtasks])

    const deleteSubtask = useCallback((subtaskId) => {
        deleteSubtaskAsync(subtaskId)
            .then(deleteSubtaskArray(subtaskId));
    }, [deleteSubtaskArray])

    return (
        <>
            {isLoading &&
                <span>Загрузка...</span>}
            {!isLoading &&
                <div className="subtaskblock"
                    onMouseEnter={() => setMouseOverDiv(true)}
                    onMouseLeave={() => setMouseOverDiv(false)}>
                    <input
                        className={doneOrNote()}
                        type="text"
                        onKeyPress={(event) => updateSubtaskTitle(subtask.id, event)}
                        onBlur={(event) => updateSubtaskTitle(subtask.id, event)}
                        defaultValue={subtask.title} />
                    {mouseOverDiv &&
                        <div className="buttons">
                            <button className="editbutton" onClick={() => changeSubtaskStatus(subtask.id)}>
                                <img src={tickImg} alt="Статус" />
                            </button>
                            <button className="deletebutton"
                                onClick={() => deleteSubtask(subtask.id)}>
                                <img src={deleteImg} alt="Удалить" />
                            </button>
                        </div>
                    }
                </div>
            }
        </>
    )
}