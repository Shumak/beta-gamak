import React, { useEffect, useState } from 'react';
import { SubtaskCreator } from "./SubtaskCreator/SubtaskCreator";
import { SubtaskBlock } from './SubtaskBlock/SubtaskBlock';

export const Subtasks = (props) => {
    const [isLoading, setIsLoading] = useState(true);
    const [subtasks, setSubtasks] = useState([]);
    
    const { deleteThisTask } = props;

    useEffect(() => {
        setIsLoading(true);
        setSubtasks(props.subtasks);
        setIsLoading(false);
    }, [props.subtasks])

    return (
        <>
            {!isLoading &&
                <div className="subtask_block">
                    {subtasks.map((subtask, index) =>
                        <SubtaskBlock
                            key={index}
                            subtask={subtask}
                            taskId={props.taskId}
                            deleteSubtaskArray={subtaskId => setSubtasks(subtasks.filter(subtask => subtask.id !== subtaskId))}
                            deleteThisTask={taskId => deleteThisTask(taskId)}
                        />
                    )}
                    <SubtaskCreator
                        taskId={props.taskId}
                        addSubtask={newSubtask => setSubtasks([...subtasks, newSubtask])}
                    />
                </div>
            }
        </>
    )
}