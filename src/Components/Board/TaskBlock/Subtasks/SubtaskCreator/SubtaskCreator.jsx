import React, { useCallback } from 'react';
import { postSubtaskAsync } from '../../../../../Services/Backend/Subtask/postSubtaskAsync';
import './SubtaskCreator.css';

export const SubtaskCreator = (props) => {
    const { taskId, addSubtask } = props;

    const pushSubtaskTitle = useCallback((event) => {
        const subtaskTitle = event.target.value;

        if (event.key === 'Enter' || event.key === undefined) {
            event.target.value = "";
            postSubtaskAsync(taskId, subtaskTitle)
                .then(json => addSubtask(json));
        }
    }, [taskId, addSubtask]);

    return (
        <div className="newSubtask">
            <input
                onKeyPress={pushSubtaskTitle}
                placeholder="Добавить подзадачу">
            </input>
        </div>

    )
}