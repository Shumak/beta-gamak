import React, { useEffect, useState } from 'react';
import addTaskImg from '../../Assets/Images/addTask.svg';
import { TaskCreator } from '../TaskCreator/TaskCreator';
import { TaskBlock } from './TaskBlock/TaskBlock';
import './Board.css';

export const Board = (props) => {

    const [isLoading, setIsLoading] = useState(true);
    const [tasks, setTasks] = useState();
    const [showAddNewTaskInput, setShowAddNewTaskInput] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        setTasks(props.array);
        setIsLoading(false);
    }, [props.array])
    return (
        <>
            {!isLoading &&
                <div className={'tasks_' + props.status}>
                    <div className={'tasks_' + props.status + '_header'}>
                        <span
                            className={'tasks_' + props.status + '_header_span'}>
                            {props.title_name}
                            {props.status !== "done" &&
                                <div className="taskcreator">
                                    <button onClick={() => setShowAddNewTaskInput(!showAddNewTaskInput)}>
                                        <img src={addTaskImg} alt="Добавить" />
                                    </button>
                                </div>}

                        </span>
                    </div>
                    <div className={'tasks_' + props.status + '_list'}>
                        {tasks.map((task, index) =>
                            <TaskBlock
                                key={index}
                                className={props.status + 'task'}
                                task={task}
                                status={props.status}
                                deleteTaskArray={taskId => setTasks(tasks.filter(task => task.id !== taskId))}
                                updateTaskArray={(taskId, updateTask) => setTasks(tasks.splice(1, taskId, updateTask))}
                            />
                        )}
                        {showAddNewTaskInput &&
                            <TaskCreator
                                status={props.status}
                                addNewTask={newTask => {
                                    setTasks([...tasks, newTask]);
                                    setShowAddNewTaskInput(false)
                                }} />
                        }
                    </div>
                </div>
            }
        </>
    );
}