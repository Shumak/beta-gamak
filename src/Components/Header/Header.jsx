import React, { useCallback, useEffect, useState } from 'react';
import { HomeRoute } from '../../Routes';
import logo from '../../Assets/Images/logo.png';
import { useIsAuthorized } from '../../Hooks/useIsAuthorized';
import Authorization from '../../Assets/Images/authorization.svg';
import Registration from '../../Assets/Images/registration.svg';
import { MainMenu } from '../MainMenu/MainMenu';
import { Identification } from '../Identification/Identification';
import { WindowForm } from '../WindowForm/WindowForm';
import './Header.css';

export const Header = () => {
    const [nameform, SetNameForm] = useState("");
    const [statevisible, SetVisible] = useState("none");
    const [imageForm, SetImageForm] = useState("");
    const [login, setLogin] = useState();

    const isAuthorized = useIsAuthorized();

    const WhatForm = useCallback((name) => {
        if (name === "Авторизация") {
            SetNameForm("Авторизация");
            SetImageForm(Authorization);
        }
        if (name === "Регистрация") {
            SetNameForm("Регистрация");
            SetImageForm(Registration);
        }
        SetVisible("flex");
    }, []);

    const closeForm = useCallback(() => {
        SetVisible("none");
    }, []);

    useEffect(() => {
        if (isAuthorized) {
            setLogin(true);
        } else {
            setLogin(false);
        }
    }, [isAuthorized, login])

    return (
        <>
            <header>
                <a href={HomeRoute}><img src={logo} alt="Logo" className="logo" /></a>
                {login &&
                    <MainMenu
                        checkexit={status => setLogin(status)}
                    />}
                {!login &&
                    <Identification
                        items={["Авторизация", "Регистрация"]}
                        FormsNames={WhatForm}
                    />}
            </header>
            <WindowForm
                whatform={nameform}
                visible={statevisible}
                image={imageForm}
                closeForm={closeForm}
            />
        </>
    )
}