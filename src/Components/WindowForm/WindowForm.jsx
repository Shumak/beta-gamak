import React from 'react';
import "./WindowForm.css";
import { Authorization } from "./Authorization/Authorization";
import { Registration } from "./Registration/Registration";

export const WindowForm = (props) => {
  
    return (
        <>
            <div className="gray" style={{ display: props.visible }} onClick={() => props.closeForm()}> </div>
            <div className="window" style={{ display: props.visible }}>
                <div className="window_block">
                    <img src={props.image} alt={props.whatform} />
                    <div className="right_side">
                        <div className="form_block">
                            <h2>{props.whatform}</h2>
                            {props.whatform === "Авторизация" &&
                                <Authorization
                                    closeForm={props.closeForm}
                                />}
                            {props.whatform === "Регистрация" &&
                                <Registration
                                    closeForm={props.closeForm}
                                />}
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}