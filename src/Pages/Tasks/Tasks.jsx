import React, { useCallback, useEffect, useState } from 'react';
import { fetchTasksAsync } from "../../Services/Backend/fetchTasksAsync";
import { Board } from "../../Components/Board/Board";
import { getUserId } from '../../Services/Login/getUserId';
import './Tasks.css';

export const Tasks = () => {

    const [isLoading, setIsLoading] = useState(true);
    const [done, setDoneTasks] = useState([]);
    const [active, setActiveTasks] = useState([]);
    const [noactive, setNoactiveTasks] = useState([]);

    /*          Разделение JSON на три массива (Выполнено, Активно, Не распределено)            */
    const divideTasks = useCallback((tasks) => {
        const doneTasks = [];
        const activeTasks = [];
        const noactiveTasks = [];

        const today = new Date();
        today.setHours(0,0,0,0);
        tasks.forEach(task => {
            if (task.deadline === null) {
                noactiveTasks.push(task);
            } else {
                const deadline = new Date(task.deadline);
                if (deadline >= today) {
                    activeTasks.push(task);
                } else if (deadline < today) {
                    doneTasks.push(task);
                }
            }
        });
        setDoneTasks(doneTasks);
        setActiveTasks(activeTasks);
        setNoactiveTasks(noactiveTasks);
    }, []);

    useEffect(() => {
        setIsLoading(true)
        fetchTasksAsync(getUserId())
            .then(tasks => {
                divideTasks(tasks);
            });
        setIsLoading(false);
    }, [divideTasks]);

    return (
        <div className="content">
            {isLoading &&
                <span>Загрузка...</span>
            }
            {!isLoading &&
                <div className="tasks_table">
                    <Board
                        array={done}
                        status={"done"}
                        title_name={"Просрочено"}
                    />
                    <Board
                        array={active}
                        status={"active"}
                        title_name={"Активно"}
                    />
                    <Board
                        array={noactive}
                        status={"noactive"}
                        title_name={"Не распределено"}
                    />
                </div>
            }
        </div>
    );
}