import React from "react";
import background from '../../Assets/Images/background.svg';
import "./Home.css";

export const Home = () => {

  return (
    <main>
      <div className="main_info">
        <h1>Управляй задачами</h1>
        <p>
          <strong>Создавай</strong> свою команду для реализации проекта.<br />
          <strong>Разделяй</strong> задачи на подзадачи.<br />
          <strong>Контролируй</strong> весь процесс самостоятельно.
          </p>
      </div>
      <img src={background} alt="background" />
    </main>
  );
}