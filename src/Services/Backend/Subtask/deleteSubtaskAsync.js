export const deleteSubtaskAsync = (subtaskId) => {
    return fetch(`http://localhost:1337/subtasks/${subtaskId}`, {
        method: "delete",
    }).then(response => response.json());
}