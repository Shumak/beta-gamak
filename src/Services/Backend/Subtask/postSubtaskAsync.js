export const postSubtaskAsync = (taskId, subtaskTitle) => {
    return fetch(`http://localhost:1337/subtasks`, {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "title": subtaskTitle,
            "task": {
                "id" : taskId
            }
        })
    }).then(response => response.json());
}