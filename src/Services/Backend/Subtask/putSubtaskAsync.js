export const putSubtaskAsync = (subtaskId, subtaskTitle) => {
    return fetch(`http://localhost:1337/subtasks/${subtaskId}`, {
        method: "PUT",
        mode: "cors",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "title": subtaskTitle,
        })
    }).then(response => response.json());
}