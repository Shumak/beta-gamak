export const fetchSubtasksAsync = async (taskId) => {
    return fetch(`http://localhost:1337/subtasks?task.id=${taskId}`)
        .then(response => response.json());
}