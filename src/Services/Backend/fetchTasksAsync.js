export const fetchTasksAsync = async (userId) => {
    return fetch(`http://localhost:1337/tasks?user.id=${userId}`)
        .then(response => response.json());
}