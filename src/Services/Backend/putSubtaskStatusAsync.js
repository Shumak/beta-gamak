export const putSubtaskStatusAsync = (subtaskId, done) => {
    return fetch(`http://localhost:1337/subtasks/${subtaskId}`, {
        method: "PUT",
        mode: "cors",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "done": done,
        })
    }).then(response => response.json());
}