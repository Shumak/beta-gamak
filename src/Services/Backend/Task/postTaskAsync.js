import {
    getUserId
} from "../../Login/getUserId";

export const postTaskAsync = (taskTitle, deadline) => {
    return fetch(`http://localhost:1337/tasks`, {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "title": taskTitle,
            "deadline": deadline,
            "user": getUserId()
        })
    }).then(response => response.json());
}