export const putTaskAsync = (taskId, taskTitle, deadline) => {
    return fetch(`http://localhost:1337/tasks/${taskId}`, {
        method: "PUT",
        mode: "cors",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "title": taskTitle,
            "deadline": deadline
        })
    }).then(response => response.json());
}