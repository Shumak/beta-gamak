export const deleteTaskAsync = (taskId) => {
    return fetch(`http://localhost:1337/tasks/${taskId}`, {
        method: "delete",
    }).then(response => response.json());
}