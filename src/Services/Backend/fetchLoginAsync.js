export const fetchLoginAsync = (identifier, password) => {
    return fetch("http://localhost:1337/auth/local", {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            identifier,
            password
        })
    })
}