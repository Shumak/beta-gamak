import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Header } from './Components/Header/Header';
import { useIsAuthorized } from './Hooks/useIsAuthorized';
import { Home } from './Pages/Home/Home';
import { Calendar } from './Pages/Calendar/Calendar';
import { Tasks } from './Pages/Tasks/Tasks';
import { HomeRoute, TasksRoute, CalendarRoute} from './Routes';

import { Footer } from './Components/Footer/Footer';

import './App.css';

export const App = () => {

  const isAuthorized = useIsAuthorized();

  return (
    <div className="wrapper">
      <BrowserRouter>
        <Header />
        <Switch>
          <Route path={HomeRoute} exact component={Home} />
          {isAuthorized === true &&
            <>
              <Route path={CalendarRoute} component={Calendar} />
              <Route path={TasksRoute} component={Tasks} />
            </>
          }
        </Switch>
      </BrowserRouter>
        <Footer />
    </div>
  );
}